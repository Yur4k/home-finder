import {StackNavigator} from "react-navigation";
import SearchScreen from "./screens/search";
import SearchResultScreen from "./screens/search-result";
import DetailScreen from "./screens/detail";
import FavesScreen from "./screens/faves";


export default RootNavigator = StackNavigator({
    Search: {
        screen: SearchScreen,
    },
    SearchResult: {
        screen: SearchResultScreen
    },
    Detail: {
        screen: DetailScreen
    },
    Faves: {
        screen: FavesScreen
    }
}, {
    initialRouteName: 'Search',
    headerMode: 'none'
});