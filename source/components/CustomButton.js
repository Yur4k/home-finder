import React, {Component} from 'react';
import {Text, TouchableOpacity, StyleSheet} from 'react-native';


class CustomButton extends Component {
    render() {
        let {container, text} = this.props.customStyle;
        let {onPress} = this.props;

        return (
            <TouchableOpacity onPress={onPress} style={[styles.ButtonContainer, container ? [...container] : null]}>
                <Text style={[styles.ButtonText, text ? text : null]}>{this.props.title}</Text>
            </TouchableOpacity>
        )
    }
}

CustomButton.defaultProps = {
    customStyle: {}
};

export default CustomButton;

const styles = StyleSheet.create({
    ButtonContainer: {
        alignSelf: 'flex-start',
        backgroundColor: '#3c5875',
        marginRight: 12,
    },
    ButtonText: {
        color: 'white',
        textAlign: 'center',
        paddingTop: 5,
        paddingRight: 15,
        paddingBottom: 5,
        paddingLeft: 15,
        fontSize: 16,
    },
});