import React from 'react';
import {View} from "react-native";
import EStyleSheet from "react-native-extended-stylesheet";
import Header from "./Header";

export default ScreenView = (props) => {
    return (
        <View style={styles.view} horizontal={false}>
            <Header title={props.title} center={props.center} favesOnPress={props.favesOnPress} favesButton={props.favesButton}/>
                <View style={styles.content}>
                    { props.children }
                </View>
        </View>
    )
};

const styles = EStyleSheet.create({
    view: {
        //backgroundColor: 'white'
    },
    content: {
        //borderWidth: 1,
        borderTopWidth: 0,
        marginLeft: '3%',
        width: '94%',
        height: '91% + 9'
    }
});
