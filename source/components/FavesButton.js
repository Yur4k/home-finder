import React, {Component} from 'react';
import {Text, TouchableOpacity, StyleSheet} from 'react-native';

export const FAVES_STATUS = {
    HIDDEN: 0,
    VISIBLE: 1,
    EXISTS: 2,
    LIST: 3,
};

export default class FavesBtn extends Component {
    renderOfStatus(status) {
        switch (status) {
            case FAVES_STATUS.VISIBLE: return "+ Faves";
            case FAVES_STATUS.EXISTS: return "unFaves";
            case FAVES_STATUS.LIST: return "All Faves";
        }
    }

    render() {
        let {status, favesOnPress} = this.props;

        return (
            <TouchableOpacity onPress={favesOnPress} style={[styles.favesBtnContainer, status === FAVES_STATUS.EXISTS ? styles.favesBtnContainerExists : null]}>
                <Text style={[styles.favesBtnText, status === FAVES_STATUS.EXISTS ? styles.favesBtnTextExists : null]}>
                    {this.renderOfStatus(status)}
                </Text>
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    favesBtnContainer: {
        backgroundColor: '#3c5875',
        marginRight: 12,
        marginLeft: 12,
    },
    favesBtnContainerExists: {
        backgroundColor: '#eee073',
    },
    favesBtnText: {
        color: 'white',
        paddingTop: 5,
        paddingRight: 15,
        paddingBottom: 5,
        paddingLeft: 15,
        fontSize: 16,
    },
    favesBtnTextExists: {
        color: 'white',
    },
});