import React, {Component} from 'react';
import {Text, View, StyleSheet, StatusBar} from "react-native";
import FavesButton, {FAVES_STATUS} from "./FavesButton";


export default class Header extends Component {

    renderOfStatus(status) {
        switch (status) {
            case FAVES_STATUS.HIDDEN: return null; break;
            case FAVES_STATUS.VISIBLE: return <FavesButton favesOnPress={this.props.favesOnPress} status={FAVES_STATUS.VISIBLE}/>; break;
            case FAVES_STATUS.EXISTS: return <FavesButton favesOnPress={this.props.favesOnPress} status={FAVES_STATUS.EXISTS} />; break;
            case FAVES_STATUS.LIST: return <FavesButton favesOnPress={this.props.favesOnPress} status={FAVES_STATUS.LIST} />; break;
            default: return null;
        }
    }

    render() {
        let {title, favesButton, center} = this.props;

        return (
            <View style={styles.header}>
                <StatusBar hidden={true} />
                <Text numberOfLines={1} style={[styles.headerTitle, center === true ? styles.headerTitleCentered : null]}>{title}</Text>
                {this.renderOfStatus(favesButton)}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    header: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        height: 50,
        width: '100%',
        backgroundColor: '#487bb1'
    },
    headerTitle: {
        flex: 1,
        marginLeft: 20,
        color: 'white',
        fontSize: 18,
    },
    headerTitleCentered: {
        flex: 1,
        marginLeft: 0,
        textAlign: 'center'
    }
});