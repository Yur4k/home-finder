import React, {Component} from 'react';
import {Image, Text, View, ToastAndroid, ScrollView} from "react-native";
import ScreenView from "../components/ScreenView";
import CustomButton from "../components/CustomButton";
import EStyleSheet from "react-native-extended-stylesheet";
import FavesStorage from "./../utils/faves-storage";
import {FAVES_STATUS} from "./../components/FavesButton";



export default class DetailScreen extends Component {
    state = {
        inFaves: FAVES_STATUS.VISIBLE,
        checkData: null
    };

    componentWillMount() {
        let {latitude, longitude, checkData} = this.props.navigation.state.params;
        let id = `${latitude}${longitude}`;

        this.setState({id});
        this.setState({checkData});

        FavesStorage.existsInFaves(id).then((data) => {
            if (data) {
                this.setState({inFaves: FAVES_STATUS.EXISTS});
            } else {
                this.setState({inFaves: FAVES_STATUS.VISIBLE});
            }
        });
    }

    componentWillUnmount() {
        if (this.state.inFaves === FAVES_STATUS.VISIBLE) {
            if (this.state.checkData) {
                this.state.checkData();
            }
        }
    }

    addToFaves(context, id, data) {
        FavesStorage.addToFaves(id, data).then(() => {
            context.setState({inFaves: FAVES_STATUS.EXISTS});
            ToastAndroid.show("Home add to Faves", ToastAndroid.SHORT);
        });
    }

    removeFromFaves(context, id) {
        FavesStorage.removeFromFaves(id).then(() => {
            context.setState({inFaves: FAVES_STATUS.VISIBLE});
            ToastAndroid.show("Home remove from Faves", ToastAndroid.SHORT);
        });
    }

    isFaveThen(data, _else = false, ...params) {
        if (typeof data === "function") {
            let context = params[params.length -1];

            if (this.state.inFaves === FAVES_STATUS.EXISTS) {
                data(context, ...params);
            } else {
                _else(context, ...params);
            }
        } else {
            if (this.state.inFaves === FAVES_STATUS.EXISTS) {
                return data;
            } else {
                return _else;
            }
        }
    }

    render() {
        let data = this.props.navigation.state.params;
        let {title, img_url, price_formatted, bathroom_number, bedroom_number, summary} = data;


        return (
            <ScreenView title="Detail view"
                        caller={"call: Detail component"}
                        center={false}
                        favesButton={this.state.inFaves}
                        favesText={this.props.propsFavesText}
                        favesOnPress={() => this.isFaveThen(this.removeFromFaves, this.addToFaves, this.state.id, data, this)}>
                <ScrollView>
                    <View style={styles.detailContainer}>
                        <Text style={styles.detailContainerPrice}>{price_formatted}</Text>
                        <Text style={styles.detailContainerAddress}>{title}</Text>
                        <Image style={styles.detailContainerImage} source={{uri: img_url}}/>
                        <Text style={styles.detailContainerInfo}>
                            Beds: {bedroom_number === "" ? 0 : bedroom_number},
                            Bathrooms: {bathroom_number === "" ? 0 : bathroom_number}</Text>
                        <Text style={styles.detailContainerDesc}>{summary}</Text>

                        <CustomButton
                            title={this.isFaveThen("Remove from faves", "Add to faves")}
                            customStyle={{
                                container: [
                                    styles.detailContainerButtonContainer,
                                    this.isFaveThen(styles.detailContainerButtonContainerExists)
                                ],
                                text: styles.detailContainerButtonText,
                            }}
                            onPress={() => this.isFaveThen(this.removeFromFaves, this.addToFaves, this.state.id, data, this)}
                        />
                    </View>
                </ScrollView>
            </ScreenView>
        )
    }
}

const styles = EStyleSheet.create({
    detailContainer: {},
    detailContainerPrice: {
        fontSize: 25,
        fontWeight: 'bold',
        marginTop: 15,
    },
    detailContainerAddress: {
        marginTop: 20,
        marginBottom: 10,
        fontSize: 20,
    },
    detailContainerImage: {
        height: 300,
    },
    detailContainerInfo: {
        marginTop: 10,
        marginBottom: 10,
        fontSize: 19,
    },
    detailContainerDesc: {
        fontSize: 19,
        marginBottom: 20,
    },
    detailContainerButtonContainer: {
        marginTop: 10,
        backgroundColor: 'green',
        width: '100%',
    },
    detailContainerButtonContainerExists: {
        backgroundColor: '#eee073',
    },
    detailContainerButtonText: {
        fontSize: 20,
    }
});