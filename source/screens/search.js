import React, {Component} from 'react';
import {
    Text,
    View,
    TextInput,
    FlatList,
    TouchableOpacity,
    ScrollView,
} from "react-native";
import EStyleSheet from "react-native-extended-stylesheet";
import ScreenView from "../components/ScreenView";
import CustomButton from "../components/CustomButton";
import {FAVES_STATUS} from "../components/FavesButton";
import RecentStorage from "../utils/recent-storage";
import {ToastAndroid} from "react-native";


export default class SearchScreen extends Component {
    originUrl = 'http://api.nestoria.co.uk/api?country=uk&pretty=1&action=search_listings&encoding=json&listing_type=buy';
    SEARCH_TYPE = {
        byPlaceName: 0,
        byMyLocation: 1
    };
    state = {
        loading: false,
        recentSearch: [],
        placeNameText: '',
        actionUrl: '',
        coords: {},
        searchType: this.SEARCH_TYPE.byPlaceName,
        getSearchType: () => Object
            .keys(this.SEARCH_TYPE)
            .find(key => this.SEARCH_TYPE[key] === this.state.searchType)
    };
    urlPlaceName = (placeName) => `&place_name=${encodeURI(placeName)}`;
    urlMyLocation = (coords) => `&centre_point=${coords.latitude},${coords.longitude}`;

    componentWillMount() {
        RecentStorage.getRecents().then((data) => {
            if (data) {
                this.setState({
                    recentSearch: data
                        .map(item => JSON.parse(item[1]))
                        .map(item => {
                            return {
                                data: item.data,
                                searchType: item.searchType,
                                count: item.count,
                                url: item.url
                            }
                        })
                });
            }
        })
    }

    setFetchUrlByType(data, type) {
        switch (type) {
            case this.SEARCH_TYPE.byPlaceName:
                this.state.actionUrl = this.urlPlaceName(data);
                break;
            case this.SEARCH_TYPE.byMyLocation:
                this.state.actionUrl = this.urlMyLocation(data);
                break;
        }
    }

    search(data, type) {
        if (this.state.loading) {
            ToastAndroid.show("Please wait a few second...", ToastAndroid.SHORT);

            return false;
        } else {

            ToastAndroid.show("Search sales...", ToastAndroid.LONG);
            this.setState({loading: true});
        }

        this.setFetchUrlByType(data, type);
        this.setState({searchType: type});

        fetch(`${this.originUrl}&page1${this.state.actionUrl}`)
            .then(response => response.json())
            .then((response) => {
                this.setState({loading: false});
                if (!response.response.listings.length) {
                    ToastAndroid.show("Nothing to found, 0 matches", ToastAndroid.SHORT);
                    this.setState({loading: true});

                    return Promise.reject("Nothing to found");
                }

                let objToRecent = {
                    id: response.request.location,
                    searchType: this.state.getSearchType(),
                    title: response.response.locations[0].long_title,
                    data: response.response.listings,
                    count: response.response.total_results,
                    url: `${this.originUrl}&page=#PAGE_NUM#${this.state.actionUrl}`
                };

                RecentStorage.addToRecents(objToRecent).then((objToRecent) => {
                    this.setState({
                        recentSearch: this.state.recentSearch.concat([objToRecent])
                    });
                    this.props.navigation.navigate('SearchResult', {
                        data: response.response,
                        urlForPagination: (page) => `${this.originUrl}&page=${page}${this.state.actionUrl}`
                    });
                })
            })
            .catch(() => {
                ToastAndroid.show("Something wrong...", ToastAndroid.SHORT);
                this.setState({loading: false});
            })
    }

    searchWithPlaceName() {
        this.search(this.state.placeNameText, this.SEARCH_TYPE.byPlaceName);
    }

    searchWithMyLocation({fake}) {
        if (fake) { //TODO Center of London
            coords = {
                latitude: 51.532589,
                longitude: -0.225221
            };

            this.setState({coords});
            this.search(coords, this.SEARCH_TYPE.byMyLocation);
        } else { //TODO Only on Real device (not working in Android Emulator but work in real device)
            console.log('___');
            navigator.geolocation.getCurrentPosition((position) => {
                console.log('LOADING NATIVE GPS COORDS');
                this.setState({coords: position.coords});
                this.search(position.coords, this.SEARCH_TYPE.byMyLocation);
            },() => {
                ToastAndroid.show("Can't found your location", ToastAndroid.SHORT);
            }, {
                enableHighAccuracy: false,
                timeout: 10000
            });
        }
    }

    toFavesScreen() {
        this.props.navigation.navigate('Faves');
    }

    loadRecent(item) {
        this.props.navigation.navigate('SearchResult', {...item});
    }

    reset() {
        if (this.state.recentSearch.length) {
            RecentStorage.resetReents().then(() => {
                this.setState({recentSearch: []})
            });
        }
    }

    _keyExtractor = (item, index) => {
        return index;
    };

    render() {
        return (
            <ScreenView title="Search page"
                        favesButton={FAVES_STATUS.LIST}
                        favesOnPress={() => this.toFavesScreen()}
            >
                <Text style={styles.title}>
                    Use the form below to search for houses to buy. default.
                    You can search by place-name, postcode or click 'My location', to search in your current location.
                </Text>
                <TextInput placeholder="Write something here..."
                           underlineColorAndroid='transparent'
                           style={styles.textInput}
                           onChangeText={(placeName) => this.setState({placeNameText: placeName})}
                />
                <View style={styles.buttonGroup}>
                    <CustomButton title="Go" onPress={() => this.searchWithPlaceName()}/>
                    <CustomButton title="My location" onPress={() => this.searchWithMyLocation({fake: false})}/>
                    <CustomButton title="fake London gps" onPress={() => this.searchWithMyLocation({fake: true})}/>
                </View>
                <ScrollView style={styles.bottomContainer}>
                    <View style={styles.bottomTitleContainer}>
                        <Text style={styles.bottomContainerText}>Recent searches:</Text>
                        <CustomButton onPress={() => this.reset()} title="RESET"/>
                    </View>

                    <FlatList
                        style={styles.bottomContainerList}
                        data={this.state.recentSearch}
                        keyExtractor={this._keyExtractor}
                        renderItem={({item, index}) => (
                            <TouchableOpacity
                                style={[styles.bottomContainerListItem, ++index === this.state.recentSearch ? styles.bottomContainerListItemLast : []]}
                                onPress={() => this.loadRecent(item)}
                            >
                                <Text style={styles.bottomContainerListItemText}>#{index} {item.searchType} {item.count}
                                    matches.</Text>
                            </TouchableOpacity>
                        )}
                    />
                </ScrollView>
            </ScreenView>
        )
    }
}

const styles = EStyleSheet.create({
    title: {
        marginTop: 35,
        marginBottom: 15,
        fontSize: 17,
        color: '#525252',
    },
    textInput: {
        fontSize: 17,
        borderBottomWidth: 1,
        paddingLeft: 5,
        marginBottom: 5
    },
    buttonGroup: {
        flexDirection: 'row'
    },
    bottomTitleContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    bottomContainer: {
        marginTop: 25,
    },
    bottomContainerList: {
        width: '100%',
        borderWidth: 0,
        borderBottomWidth: 0
    },
    bottomContainerText: {
        fontSize: 17,
        color: '#525252',
    },
    bottomContainerListItem: {
        margin: 7,
        marginBottom: 0,
        backgroundColor: '#487bb1',
    },
    bottomContainerListItemLast: {
        marginBottom: 3
    },
    bottomContainerListItemText: {
        color: 'white',
        fontSize: 19,
        padding: 6,
    }
});