import React, {Component} from 'react';
import {FlatList, Image, Text, TouchableOpacity, View} from "react-native";
import ScreenView from "../components/ScreenView";
import EStyleSheet from "react-native-extended-stylesheet";


export default class SearchResultScreen extends Component {
    state = {
        url: '',
        data: [],
        totalResults: 0,
        page: 1,
    };

    componentWillMount() {
        let {data, urlForPagination, url, count} = this.props.navigation.state.params;

        if (!urlForPagination) {
            this.setState({url: url});

            urlForPagination = (page) => this.state.url.replace(/#PAGE_NUM#/i, page);
        }

        this.setState({
            data: url ? data : data.listings,
            totalResults: url ? count : data.total_results,
            page: 1,
            totalPages: data.total_pages,
            urlForPagination: urlForPagination
        });
    }

    onItemTouch(item) {
        this.props.navigation.navigate('Detail', {...item});
    }

    loadMore() {
       fetch(this.state.urlForPagination(++this.state.page))
            .then(response => response.json())
            .then(({response}) => response)
            .then(data => {
                this.setState({
                    data: this.state.data.concat(...data.listings)
                });
            });
    }

    currentResults() {
        return this.state.data.length > this.state.totalResults ? this.state.totalResults : this.state.data.length;
    }

    render() {
        return (
            <ScreenView title={`${this.currentResults()} of ${this.state.totalResults} matches`}
                        center={true}
                        favesButton={true}
            >
                    <FlatList
                        onEndReachedThreshold={0.5}
                        onEndReached={() => this.loadMore()}
                        shouldItemUpdate={() => false}
                        style={styles.searchResultList}
                        data={this.state.data}
                        keyExtractor={(item) => item.lister_url}
                        renderItem={({item, index}) => (
                            <TouchableOpacity style={styles.searchResultListItem} onPress={() => this.onItemTouch(item)}>
                                <Image style={styles.searchResultListItemImage} source={{uri: item.thumb_url}} />
                                <View style={styles.searchResultListItemContainer}>
                                    <Text style={styles.searchResultListItemPrice}>{item.price_formatted}</Text>
                                    <Text style={styles.searchResultListItemDesc}>{item.title}</Text>
                                </View>
                          </TouchableOpacity>
                        )}
                    />
            </ScreenView>
        )
    }
}

export const styles = EStyleSheet.create({
    searchResultList: {
    },
    searchResultListItem: {
        flexDirection: 'row',
        marginTop: 7,
        borderWidth: 1,
        borderColor: '#d6d6d6',
        borderLeftWidth: 3,
        borderLeftColor: '#6d9cff',
    },
    searchResultListItemImage: {
        width: 90,
        height: 90,
        margin: 5,
    },
    searchResultListItemContainer: {
        flex: 1,
        padding: 10,
        paddingLeft: 5,
    },
    searchResultListItemPrice: {
        paddingBottom: 7,
        fontSize: 18,
    },
    searchResultListItemDesc: {
        flex: 1,
        fontSize: 17,
    }
});