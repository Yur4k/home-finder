import React, {Component} from 'react';
import {FlatList, Image, Text, TouchableOpacity, View} from "react-native";
import FavesStorage from "../utils/faves-storage";
import ScreenView from "../components/ScreenView";
import {styles as exStyles}  from "./search-result";


export default class FavesScreen extends Component {
    state = {
        data: [],
        dataValidator: ''
    };

    componentWillMount() {
        FavesStorage.getFaves().then(data => {
            this.setState({data: data.map(item => JSON.parse(item[1]))});
        });
    }

    checkData() {
        FavesStorage.getFaves().then(data => {
            this.setState({data: data.map(item => JSON.parse(item[1]))});
        });
    }

    onItemTouch(item) {
        this.props.navigation.navigate('Detail', {
            ...item,
            checkData: () => this.checkData()
        });
    }

    render() {
        return (
            <ScreenView title="List of Faves" center={true} favesButton={false}>
                <Text style={{display: 'none'}}>{this.state.dataValidator}</Text>
                <FlatList
                    style={exStyles.searchResultList}
                    data={this.state.data}
                    keyExtractor={(item) => item.lister_url}
                    renderItem={({item, index}) => (
                        <TouchableOpacity style={exStyles.searchResultListItem} onPress={() => this.onItemTouch(item)}>
                            <Image style={exStyles.searchResultListItemImage} source={{uri: item.thumb_url}} />
                            <View style={exStyles.searchResultListItemContainer}>
                                <Text style={exStyles.searchResultListItemPrice}>{item.price_formatted}</Text>
                                <Text style={exStyles.searchResultListItemDesc}>{item.title}</Text>
                            </View>
                        </TouchableOpacity>
                    )}
                />
            </ScreenView>
        )
    }
};