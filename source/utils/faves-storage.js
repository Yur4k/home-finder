import {AsyncStorage} from 'react-native'


export default class FavesStorage {
    static storeName =  "@FavesStorage";
    static key = (id) => `:id?${id}`;

    static async getFaves(){
        let keys = await AsyncStorage.getAllKeys();

        keys = keys.filter(key => {
            return key.indexOf(FavesStorage.storeName) === 0
        });

        return AsyncStorage.multiGet(keys);
    }

    static async removeFromFaves(id){
        return AsyncStorage.removeItem(`${FavesStorage.storeName}${FavesStorage.key(id)}`);
    }

    static async existsInFaves(id){
        return await AsyncStorage.getItem(`${FavesStorage.storeName}${FavesStorage.key(id)}`) !== null;
    }

    static async addToFaves(id, data){
        return AsyncStorage.setItem(`${FavesStorage.storeName}${FavesStorage.key(id)}`, JSON.stringify(data));
    }
}