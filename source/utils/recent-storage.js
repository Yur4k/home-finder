import {AsyncStorage} from 'react-native'
import FavesStorage from "./faves-storage";


export default class RecentStorage {
    static storeName =  "@RecentStorage";
    static key = (id) => `:id?${id}`;


    static async getRecents(){
        let keys = await AsyncStorage.getAllKeys();

        keys = keys.filter(key => key.indexOf(RecentStorage.storeName) === 0);

        return keys ? AsyncStorage.multiGet(keys) : [];
    }

    static async resetReents() {
        let keys = await AsyncStorage.getAllKeys();

        keys = await keys.filter(key => key.indexOf(RecentStorage.storeName) === 0);
        await AsyncStorage.multiRemove(keys);

        return true;
    }

    static async removeFromRecents(id){
        return AsyncStorage.removeItem(`${RecentStorage.storeName}${RecentStorage.key(id)}`);
    }

    static async existsInRecents(id){
        return await AsyncStorage.getItem(`${RecentStorage.storeName}${RecentStorage.key(id)}`) !== null;
    }

    static async addToRecents(itemData){
        let {id, searchType, title, data, count, url} = itemData;

        await AsyncStorage.setItem(`${RecentStorage.storeName}${RecentStorage.key(id)}`, JSON.stringify({
            searchType, title, count, data, url
        }));

        return itemData;
    }
}