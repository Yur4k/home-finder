import React from 'react';
import EStyleSheet from "react-native-extended-stylesheet";
import RootNavigator from "./source/Navigator";


class App extends React.Component{
    render() {
        return (
            <RootNavigator/>
        )
    }
}

EStyleSheet.build();

export default App;